iot 学习

## 入门
1. [物联网(IoT)开发的入门教程](https://github.com/nladuo/IoT-Firstep)
1. iot 7大技能 . https://www.ibm.com/developerworks/community/blogs/3302cc3b-074e-44da-90b1-5055f1dc0d9c/entry/just-getting-started-iot-consider-7-key-iot-concepts-skills
1. 智能家居——IoT零基础入门篇 . https://www.cnblogs.com/rainmote/p/7617454.html
2. iot 社区 . https://dev.iot.aliyun.com/
1. iot 入门 . https://zhuanlan.zhihu.com/p/55640307
1. 协议：mqtt协议，comsap协议
1. 3表:水表电表烟表，穿戴设备，电动车(芯片，模组)
1. 数据库: mongo
1. 消息中间件: kafka

## 系统学习
1. [华为物联网7天系统课程](https://education.huaweicloud.com:8443/courses/course-v1:HuaweiX+CBUCNXT007+Self-paced/courseware/422d6b524aa14c459cd04d1149236297/c63d5b7c03ab418aa01f07c1e9d66680/)

## 开源项目
1. [开源物联网系统设计](https://github.com/phodal/iot)
1. [AlibabaCloudDocs / iot](https://github.com/AlibabaCloudDocs/iot)
2. [RIOT开源物联网操作系统](https://github.com/RIOT-OS/RIOT)
1. [awesome-iot 资源总结](https://github.com/phodal/awesome-iot)
1. [IPv6-WSN-book](https://github.com/marcozennaro/IPv6-WSN-book)



## 物联网的应用

1. 物联网各领域应用项目列表 . https://zhuanlan.zhihu.com/p/29078733